PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SOIC254P1030X460-6N
$EndINDEX
$MODULE SOIC254P1030X460-6N
Po 0 0 0 15 00000000 00000000 ~~
Li SOIC254P1030X460-6N
Cd <B>DIODE</B><p>diameter 2.54 mm, horizontal, grid 10.16 mm
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -0.86 -5.435 1 1 0 0.05 N V 21 "SOIC254P1030X460-6N"
T1 -0.525 5.565 1 1 0 0.05 N V 21 "VAL**"
DS -3.525 -4.5 3.525 -4.5 0.2 21
DS 3.525 -4.5 3.525 4.5 0.2 21
DS 3.525 4.5 -3.525 4.5 0.2 21
DS -3.525 4.5 -3.525 -4.5 0.2 21
DS -5.8 -4.75 5.8 -4.75 0.05 24
DS 5.8 -4.75 5.8 4.75 0.05 24
DS 5.8 4.75 -5.8 4.75 0.05 24
DS -5.8 4.75 -5.8 -4.75 0.05 24
DC -2.5 -3.5 -2.13945 -3.5 0.2 21
DP 0 0 0 0 4 0.2 21
Dl -2.8 -3.5
Dl -2.5 -3.8
Dl -2.2 -3.5
Dl -2.5 -3.2
DC -4.7 -3.9 -4.6 -3.9 0.2 21
$PAD
Sh "1" R 1.7 1.45 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.7 -2.54
$EndPAD
$PAD
Sh "2" R 1.7 1.45 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.7 0
$EndPAD
$PAD
Sh "3" R 1.7 1.45 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.7 2.54
$EndPAD
$PAD
Sh "4" R 1.7 1.45 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.7 2.54
$EndPAD
$PAD
Sh "5" R 1.7 1.45 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.7 0
$EndPAD
$PAD
Sh "6" R 1.7 1.45 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.7 -2.54
$EndPAD
$EndMODULE SOIC254P1030X460-6N
